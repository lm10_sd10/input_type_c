# Input_Type_C
### Hi guys I am SD...And this is my first C project...
*When I entered the C I found a real problem that is that when I take input from the user there is no way to check whether it is 
of integer type or float type or char type..So I decided to write my own simple function to make it simple...*

## And here I am introducing `dtype( char )` function...
It is so simple ...It takes a char or string as a parameter ..In return it returns a single char to help you determine which type
it was....
*Here's a simple example : *<br />
```C
char input[50];
gets(input);
char type=dtype(input);
if ( type=='i' ) { printf("It is an INTEGER..."); }
else if ( type=='f' ) { printf("It is a FLOAT OR DECIMAL"); }
else if ( type=='c' ) { printf("It is a CHAR...") }

//The return types are : 
// i for integer..
// c for char..
// f for float or decimals..

```

### If you have any issue related to it or found any bug please contact me by WhatsApp : 8617397380
## Finally it is free...You can do whatever you wanted to...
## Thanks For Reading...